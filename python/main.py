# Libraries
import sys
import json
from tkinter import filedialog as fd

import pygame, pygame.midi as pmidi
import vgamepad as vg
from pynput.keyboard import Controller as K_Controller
from pynput.mouse import Controller as M_Controller

# Local
from translator import Translator

def main():
    pygame.init()
    pmidi.init()

    # Initialize device controllers 
    devices = {"keyboard": K_Controller(), "mouse": M_Controller(), "gamepad": vg.VX360Gamepad()}

    # Initialize list of translators
    translators = []

    # Initialize dictionary of toggleable keys
    toggled_keys = {}

    joystick_pos = {"left": {"x": 0.0, "y": 0.0}, "right": {"x": 0.0, "y": 0.0}}

    if len(sys.argv) > 1:
        independent = eval(sys.argv[1])
    else:
        independent = True

    if independent:
        input("Press enter to select config file")
        # Read config file
        config_path = fd.askopenfilename()
        print("Loading", config_path)
    else:
        config_path = "../active_config.JSON"

    try:
        json_data = json.load(open(config_path))
    except Exception as e:
        print(e)
        print("Failed to parse JSON")
    else:
        translators_data = json_data["translators"]
        for key in translators_data:
            if all(value in translators_data[key] for value in Translator.REQUIRED_FIELDS):
                new_translator = Translator(translators_data[key], devices)
                translators.append(new_translator)
                if new_translator.error != "":
                    print(f"Translator {key} messages:\n{new_translator.error}")
            else:
                print(f"Translator {key} does not have all required fields")
            
        for translator in translators:
            toggled_keys[translator.toggle_key] = False

        # Select MIDI device
        print('\n' + str(pmidi.get_count()) + ' MIDI devices detected', '\n')
        for device_id in range(pmidi.get_count()):
            device_info = pmidi.get_device_info(device_id)
            info_string = str(device_id+1) + ". " + str(device_info[1])[2:-1]
            if device_info[2] == 1:
                info_string = info_string + " Input"
            if device_info[3] == 1:
                info_string = info_string + " Output"
            print(info_string)

        device_id = int(input("\nSelect MIDI device: ")) - 1
        try:
            midi_device = pmidi.Input(device_id)
            print(pmidi.get_device_info(device_id), "selected")
        except Exception as e:
            print(e)
            print("Failed to open MIDI device.  Check that MIDI device is not already in use.\n")
        else:
            while True:

                # Read MIDI input
                if midi_device.poll():
                    event = midi_device.read(1)[0][0]
                    #print(event)
                
                    #TODO: Make translator.run()
                    # Translate and execute MIDI input
                    for translator in translators:
                        #print("Checking", translator.name)
                        translator.run(event, toggled_keys, joystick_pos)
        finally:
            pmidi.quit()


if __name__ == "__main__":
    main()
