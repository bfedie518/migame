from enum import Enum, auto
from threading import Timer
from pynput.keyboard import Key, Controller
from pynput.mouse import Button, Controller
from vgamepad import XUSB_BUTTON as vg


class Device(Enum):
    KEYBOARD = auto()
    GAMEPAD = auto()
    MOUSE = auto()

class Action(Enum):
    PRESS = auto()
    RELEASE = auto()
    TOGGLE = auto()
    TAP = auto()


class Translator:

    REQUIRED_FIELDS = ["name", "status", "note", "device", "key", "action"]

    # self.name: String
    # self.status: int
    # self.note: int
    # self.velocity = {min: int, max: int}
    # self.device: Device enum (int)
    # self.action: Action enum (int)
    # self._key: String
    # self.toggle_key: String # Add joystick_pos to key when relevant
    # self.error: String
    # self.devices: {keyboard, mouse, gamepad}
    # self.joystick_pos: {x, y}


    def __init__(self, data, devices):
        self.error = ""
        self.devices = devices

        self.name = data["name"]
        self.status = data["status"]
        self.note = data["note"]
        self.velocity = {}

        if "velocity" in data:
            if "min" in data["velocity"]:
                self.velocity["min"] = data["velocity"]["min"]
            else:
                self.velocity["min"] = 0
                self.error += "No min velocity--default to 0. \n"
            if "max" in data["velocity"]:
                self.velocity["max"] = data["velocity"]["max"]
            else:
                self.velocity["max"] = 127
                self.error += "No max velocity.  Default to 127. \n"
        else:
            self.velocity["min"] = 0
            self.velocity["max"] = 127
            self.error += "No min velocity.  Default to 0. \n"
            self.error += "No max velocity.  Default to 127. \n"

        self.device = eval("Device." + data["device"].upper()) 

        match self.device:
            case Device.KEYBOARD:
                if len(data["key"]) == 1:
                    self._key = data["key"]
                else:
                    self._key = "Key." + data["key"]
            case Device.MOUSE:
                # No movement yet
                self._key = "Button." + data["key"]
            case Device.GAMEPAD:
                if "trigger" in data["key"] or "joystick" in data["key"]:
                    self._key = data["key"] 
                else:
                    self._key = "vg.XUSB_GAMEPAD_" + data["key"]

        self.toggle_key = self._key

        if "joystick" in self._key:
            if "joystick_pos" in data:
                self.joystick_pos = {}

                if "x" in data["joystick_pos"]:
                    self.joystick_pos["x"] = data["joystick_pos"]["x"]
                else:
                    self.joystick_pos["x"] = 0.0
                    self.error += 'No joystick_pos["x"]--default to 0.0.\n'
                if "y" in data["joystick_pos"]:
                    self.joystick_pos["y"] = data["joystick_pos"]["y"]
                else:
                    self.joystick_pos["y"] = 0.0
                    self.error += 'No joystick_pos["y"]--default to 0.0.\n'
            else:
                self.joystick_pos["x"] = 0.0
                self.joystick_pos["y"] = 0.0
                self.error += 'No joystick_pos["x"]--default to 0.0.\n'
                self.error += 'No joystick_pos["y"]--default to 0.0.\n'
            self.toggle_key += str(self.joystick_pos)

        self.action = eval("Action." + data["action"].upper())
        if self.action == Action.TAP:
            if "tap_delay" in data:
                self.tap_delay = data["tap_delay"]
            else:
                self.tap_delay = 0.0
                self.error += "No tap delay--default to 0.0.\n"
    

    def run(self, event, key_is_toggled, joystick_pos):
        # MIDI event format:
        # [Status, Note, Velocity]

        if event[0] == self.status and event[1] == self.note and \
                event[2] > self.velocity["min"] and event[2] < self.velocity["max"]:

            function_string = ""
            output_string = ""

            match self.action:
                case Action.PRESS:
                    output_string += "press"
                case Action.RELEASE:
                    output_string += "release"
                case Action.TOGGLE | Action.TAP:
                    if key_is_toggled[self.toggle_key]:
                        output_string += "release"
                    else:
                        output_string += "press"

            function_string += output_string + "_"

            match self.device:
                case Device.KEYBOARD:
                    function_string += "keyboard()"
                case Device.GAMEPAD:
                    if "joystick" in self._key:
                        function_string += "gamepad_joystick(joystick_pos)"
                    elif "trigger" in self._key:
                        function_string += "gamepad_trigger()"
                    else:
                        function_string += "gamepad_button()"
                case Device.MOUSE:
                    function_string += "mouse_button()"

            eval("self." + function_string)
            self.devices["gamepad"].update()

            if "press" in function_string:
                key_is_toggled[self.toggle_key] = True
            elif "release" in function_string:
                key_is_toggled[self.toggle_key] = False

            if self.action == Action.TAP:
                output_string = "TAP: " + output_string
                if "press" in function_string:
                    Timer(self.tap_delay, self.run, [event, key_is_toggled, joystick_pos]).start()
            elif self.action == Action.TOGGLE:
                output_string = "TOGGLE: " + output_string

            print(output_string, self.name)


    def press_keyboard(self):
        if len(self._key) > 1:
            _key = eval(self._key)
        else:
            _key = self._key

        self.devices["keyboard"].press(_key)


    def release_keyboard(self):
        if len(self._key) > 1:
            _key = eval(self._key)
        else:
            _key = self._key

        self.devices["keyboard"].release(_key)


    def press_gamepad_button(self):
        self.devices["gamepad"].press_button(eval(self._key)) 


    def release_gamepad_button(self):
        self.devices["gamepad"].release_button(eval(self._key)) 


    def press_gamepad_trigger(self):
        eval(f"self.devices['gamepad'].{self.key}(value=255)")


    def release_gamepad_trigger(self):
        eval(f"self.devices['gamepad'].{self.key}(value=0)")


    def press_gamepad_joystick(self, joystick_pos):
        which_stick = self._key[0:self._key.find("_")]
        joystick_pos[which_stick]["x"] += self.joystick_pos["x"]
        joystick_pos[which_stick]["y"] += self.joystick_pos["y"]
        eval(f"""self.devices['gamepad'].{self._key}_float(
                x_value_float=joystick_pos[which_stick]['x'], 
                y_value_float=joystick_pos[which_stick]['x'])""")


    def release_gamepad_joystick(self, joystick_pos):
        which_stick = self._key[0:self._key.find("_")]
        joystick_pos[which_stick]["x"] -= self.joystick_pos["x"]
        joystick_pos[which_stick]["y"] -= self.joystick_pos["y"]
        eval(f"""self.devices['gamepad'].{self._key}_float(
                x_value_float=joystick_pos[which_stick]['x'], 
                y_value_float=joystick_pos[which_stick]['x'])""")


    def press_mouse_button(self):
        self.devices["mouse"].press(eval(self._key))


    def release_mouse_button(self):
        self.devices["mouse"].release(eval(self._key))

