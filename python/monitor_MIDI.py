# Libraries
import pygame, pygame.midi as pmidi

pygame.init()
pmidi.init()

# Select MIDI device
print('\n' + str(pmidi.get_count()) + ' MIDI devices detected', '\n')
for device_id in range(pmidi.get_count()):
    device_info = pmidi.get_device_info(device_id)
    info_string = str(device_id+1) + ". " + str(device_info[1])[2:-1]
    if device_info[2] == 1:
        info_string = info_string + " Input"
    if device_info[3] == 1:
        info_string = info_string + " Output"
    print(info_string)

device_id = int(input("\nSelect MIDI device: ")) - 1
midi_device = pmidi.Input(device_id)
print(pmidi.get_device_info(device_id), "selected")

print("\nFormat:\n[Type, Note, Velocity, ???]")

while True:

    # Read MIDI input
    if midi_device.poll():
        event = midi_device.read(1)[0][0]
        print(event)
 
