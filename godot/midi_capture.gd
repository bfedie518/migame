extends Control

enum Note {ON = 9, OFF = 8}

signal midi_chosen(event)

var _midi_events: Array = []

@onready var btn_start: Button = %BtnStartCapture
@onready var btn_end: Button = %BtnEndCapture
@onready var list_midi_events: ItemList = %ListMidiEvents


func _init() -> void:
	visible = false


func _input(event: InputEvent) -> void:
	if event is InputEventMIDI:
		var e := event as InputEventMIDI

		# event.channel is 0-indexed
		var midi_status: int = e.channel + 128 + (16 if e.message == Note.ON else 0)
		_midi_events.append({"status" = midi_status, "note" = e.pitch,})
		var item_string: String = "Ch: %s, Note: %s " %[e.channel + 1, e.pitch]
		item_string += "On" if e.message == Note.ON else "Off"
		list_midi_events.add_item(item_string)


func _on_btn_clear_events_pressed() -> void:
	list_midi_events.clear()
	_midi_events.clear()


func _on_list_midi_events_item_selected(index: int) -> void:
	emit_signal("midi_chosen", _midi_events[index])


func _on_btn_start_capture_pressed() -> void:
	OS.open_midi_inputs()
	btn_start.disabled = true
	btn_end.disabled = false


func _on_btn_end_capture_pressed() -> void:
	OS.close_midi_inputs()
	btn_start.disabled = false
	btn_end.disabled = true
