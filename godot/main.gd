extends Control

enum FileMenu{
	OPEN,
	SAVE,
	SAVE_AS,
}

enum WindowMenu{MIDI_CAPTURE}

var translator_scene: PackedScene = preload("res://translator.tscn")

var currently_open: String = "": set = _set_currently_open
var has_changed := false: set = _set_has_changed
var open_after_save := false

@onready var save_dialog: FileDialog = %SaveDialog
@onready var open_dialog: FileDialog = %OpenDialog
@onready var check_save_dialog: ConfirmationDialog = %CheckSaveDialog


@onready var lbl_filename: Label = %LblFilename
@onready var menu_file: PopupMenu = %MenuBar/File
@onready var menu_window: PopupMenu = %MenuBar/Window

@onready var midi_capture: Control = %MidiCapture
@onready var box_translators: Control = %Translators
@onready var btn_new_translator: Button = %BtnNewTranslator


func _ready() -> void:
	menu_file.add_item("Open", FileMenu.OPEN, KEY_O | KEY_MASK_CTRL)
	menu_file.add_item("Save", FileMenu.SAVE, KEY_S | KEY_MASK_CTRL)
	menu_file.add_item("Save As", FileMenu.SAVE_AS, KEY_S | KEY_MASK_CTRL | KEY_MASK_SHIFT)

	menu_window.add_check_item("Capture MIDI", WindowMenu.MIDI_CAPTURE, KEY_M | KEY_MASK_CTRL)


func _set_currently_open(value: String) -> void:
	currently_open = value
	var start = value.rfind("/") + 1
	var length = value.rfind(".") - start
	lbl_filename.text = value.substr(start, length)


func _set_has_changed(value: bool) -> void:
	has_changed = value
	if lbl_filename.text.contains("*"):
		if not has_changed:
			lbl_filename.text = lbl_filename.text.left(lbl_filename.text.length() - 1)
	elif has_changed:
			lbl_filename.text += "*"


func _on_file_id_pressed(id: int) -> void:
	match id:
		FileMenu.SAVE:
			if currently_open == "":
				save_dialog.popup_centered(save_dialog.size)
			else:
				_save_config(currently_open)
		FileMenu.SAVE_AS:
			save_dialog.popup_centered(save_dialog.size)
		FileMenu.OPEN:
			if not has_changed:
				open_dialog.popup_centered(open_dialog.size)
			else:
				check_save_dialog.popup_centered()


func _save_config(path: String) -> void:
	var translators := {}
	for child in box_translators.get_children():
		if child is Translator:
			var values = (child as Translator).serialize()
			translators[translators.size()] = values

	var json_data := {"translators": translators} #Add more as more data is to be saved
	var json_string := JSON.stringify(json_data, "\t")
	var file = FileAccess.open(path, FileAccess.WRITE)
	file.store_string(json_string)

	currently_open = path
	has_changed = false

	if open_after_save:
		open_dialog.popup_centered(open_dialog.size)
		open_after_save = false


func _new_translator() -> Translator:
	var new_translator: Translator = translator_scene.instantiate()
	new_translator.delete.connect(_delete)
	new_translator.move.connect(_move)
	new_translator.changed.connect(_on_change)
	new_translator.capturing_midi.connect(_on_midi_capture_pressed)
	box_translators.add_child(new_translator)
	box_translators.move_child(btn_new_translator, -1)
#	has_changed = true # Not needed
	return new_translator


func _open_config(path: String) -> void:
	currently_open = path
	var file = FileAccess.open(path, FileAccess.READ)
	var data_string = file.get_as_text()
	var json := JSON.new()
	var error = json.parse(data_string)
	if error == OK:
		if json.data.has("translators"):
			var translators: Dictionary = json.data.translators

			for child in box_translators.get_children():
				if child is Translator: child.queue_free()
			for key in translators.keys():
				if translators[key].has_all(Translator.REQUIRED_FIELDS):
					_new_translator().load_data(translators[key])
				else:
					print ("Translator %s does not have all required fields" %key)
			has_changed = false
		else:
			print("No readable translator data")
	else:
		print("Unable to parse JSON")


func _delete(translator: Translator) -> void:
	translator.queue_free()
	has_changed = true


func _move(translator: Translator, want_up: bool) -> void:

	var current_index: int = translator.get_index()
	if want_up:
		if current_index > 0:
			box_translators.move_child(translator, current_index - 1)
	elif current_index < box_translators.get_child_count() - 2:
		box_translators.move_child(translator, current_index + 1)
	has_changed = true


func _on_midi_capture_pressed() -> void:
	menu_window.set_item_checked(WindowMenu.MIDI_CAPTURE, true)
	midi_capture.visible = true


func _on_change() -> void:
	has_changed = true


func _on_check_save_dialog_confirmed() -> void:
	if currently_open == "":
		open_after_save = true
		save_dialog.popup_centered(save_dialog.size)
	else:
		_save_config(currently_open)
		open_dialog.popup_centered(open_dialog.size)


func _on_check_save_dialog_cancelled() -> void:
	open_dialog.popup_centered(open_dialog.size)


func _on_window_index_pressed(index: int) -> void:
	if menu_window.is_item_checkable(index):
		menu_window.set_item_checked(index, !menu_window.is_item_checked(index))

	if index == WindowMenu.MIDI_CAPTURE:
		midi_capture.visible = menu_window.is_item_checked(WindowMenu.MIDI_CAPTURE)
		if not midi_capture.visible:
			for item in box_translators.get_children():
				if item is Translator:
					(item as Translator).is_capturing_midi = false


func _on_btn_end_capture_pressed() -> void:
	for item in box_translators.get_children():
		if item is Translator:
			(item as Translator).is_capturing_midi = false


func _on_midi_capture_midi_chosen(event) -> void:
	for item in box_translators.get_children():
		if item is Translator:
			if item.is_capturing_midi:
				item.spn_status.value = event.status
				item.spn_note.value = event.note


func _on_btn_run_pressed() -> void:
	midi_capture._on_btn_end_capture_pressed()
	_save_config("../active_config.JSON")
	OS.create_process("python", ["../python/main.py", "False"], true)
