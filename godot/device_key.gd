extends Node

const KEYBOARD := {
	"left": "Left", "right": "Right", "up": "Up", "down": "Down", "esc": "Esc",
	"ctrl": "Ctrl", "enter": "Enter", "shift": "Shift" , "space": "Space", "tab": "Tab",

	"a": "A", "b": "B", "c": "C", "d": "D", "e": "E", "f": "F", "g": "G",
	"h": "H", "i": "I", "j": "J", "k": "K", "l": "L", "m": "M", "n": "N",
	"o": "O", "p": "P", "q": "Q", "r": "R", "s": "S", "t": "T", "u": "U",
	"v": "V", "w": "W", "x": "X", "y": "Y", "z": "Z",

	"1": "1", "2": "2", "3": "3", "4": "4", "5": "5",
	"6": "6", "7": "7", "8": "8", "9": "9", "0": "0",

	"-": "-",  "=": "=", "[": "[", "]": "]", "\\": "\\",
	";": ";", "'": "'", ",": ",", ".": ".", "/": "/", "`": "`",

	"alt": "Alt", "backspace": "Backspace", "caps_lock": "Caps Lock",
	"Cmd": "cmd", "Delete": "delete", "End": "end", "print_screen": "Print Screen",
	"home": "Home", "insert": "Insert", "page_down": "Page Down", "page_up": "Page Up",

	"f1": "F1", "f2": "F2", "f3": "F3", "f4": "F4", "f5": "F5",
	"f6": "F6", "f7": "F7" , "f8": "F8", "f9": "F9", "f10": "F10",
}

const KEYBOARD_ADVANCED := {
	"!": "!", "@": "@", "#": "#", "$": "$", "%": "%", "^": "^", "&": "&",
	"*": "*", "(": "(", ")": ")", "_": "_", "+": "+", "{": "{", "}": "}",
	"|": "|", ":": ":", "\"": "\"", "<": "<", ">": ">", "?": "?", "~": "~",

	"menu": "Menu", "num_lock": "NumLock", "pause": "Pause", "scroll_lock": "Scroll Lock",

	"Alt Gr": "alt_gr", "alt_l": "Left Alt", "alt_r": "Right Alt",
	"cmd_l": "Left Cmd", "cmd_r": "Right Cmd", "ctrl_l": "Left Ctrl",
	"ctrl_r": "Right Ctrl", "shift_l": "Left Shift", "shift_r": "Right Shift",

	"media_next": "Next Media", "media_play_pause": "Play/Pause Media",
	"media_previous": "Previous Media", "media_volume_down": "Volume Down",
	"media_volume_up": "Volume Up", "media_volume_mute": "Mute",
}

const GAMEPAD := {
	"A": "A", "B": "B", "X": "X", "Y": "Y",
	"LEFT_SHOULDER": "Left Shoulder", "RIGHT_SHOULDER": "Right Shoulder",
	"left_trigger": "Left Trigger", "right_trigger": "Right Trigger",
	"DPAD_LEFT": "DPad Left", "DPAD_RIGHT": "DPad Right",
	"DPAD_UP": "DPad Up", "DPAD_DOWN": "DPad Down",
	"left_joystick": "Left Joystick", "right_joystick": "Right Joystick",
	"START": "Start", "BACK": "Back", "GUIDE": "Guide",
}

const MOUSE := {"left": "Left Button", "right": "Right Button"}
